//vector de 6 elementos usando 3 tipos de datos
let vector = ['tres', 'amarillo', true, false, 26, 17];

// Imprimir en la consola cada valor usando "for"
console.log('Usando "for":');
for (let i = 0; i < vector.length; i++) {
  console.log(vector[i]);
}

// Idem al anterior usando "forEach"
console.log('Usando "forEach":');
vector.forEach(function (valor) {
  console.log(valor);
});

// Idem al anterior usando "map"
console.log('Usando "map":');
vector.map(function (valor) {
  console.log(valor);
});

// Idem al anterior usando "while"
console.log('Usando "while":');
let i = 0;
while (i < vector.length) {
  console.log(vector[i]);
  i++;
}

// Idem al anterior usando "for..of"
console.log('Usando "for..of":');
for (let valor of vector) {
  console.log(valor);
}
