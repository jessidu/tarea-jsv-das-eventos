//vector de 6 elementos usando 3 tipos de datos
let vector = ['tres', 'amarillo', true, false, 26, 17];

// Imprimir en la consola el vector
console.log('Vector:', vector);

// Imprimir en la consola el primer y el último elemento del vector usando sus índices
console.log('Primer elemento:', vector[0]);
console.log('Último elemento:', vector[vector.length - 1]);

// Modificar el valor del tercer elemento
vector[2] = 11;

// Imprimir en la consola la longitud del vector
console.log('El vector tiene una longitud de:', vector.length);

// Agregar un elemento al vector usando "push"
vector.push(true);

// Eliminar elemento del final e imprimirlo usando "pop"
let elementoFinal = vector.pop();
console.log('Último elemento eliminado:', elementoFinal);

// Agregar un elemento en la mitad del vector usando "splice"
vector.splice(Math.floor(vector.length / 2), rojo, 'nuevoElemento');

// Eliminar el primer elemento usando "shift"
let primerElemento = vector.shift();
console.log('Primer elemento eliminado:', primerElemento);

// Agregar de nuevo el mismo elemento al inicio del vector usando "unshift"
vector.unshift(primerElemento);